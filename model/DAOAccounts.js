var fs = require('fs');
var mysql = require('mysql');

module.exports = class DAOAccounts {
	constructor() {
		var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
		this.connection = mysql.createConnection({
			host: config.database.host,
			database: config.database.database,
			user: config.database.user,
			password: config.database.password
		});
	}

	/**
	 * Method that validate the login and password
	 * 
	 * @param {String} login the login
	 * @param {String} password the password
	 * @param {successCallback} success the callback that handles the success of authentication 
	 * @param {failedCallback} failed the callback that handles the failed of authentication
	 */
	validateIdentity(login, password, success, failed) {
		this.connection.connect();

		this.connection.query("SELECT password FROM accounts WHERE login = ?",
			[login],
			(err, rows, field) => {
				if (rows && rows.length > 0) {
					if (rows[0].password == password) {
						success();
					} else {
						failed();
					}
				} else {
					failed();
				}
			});

		this.connection.end();
	}

	/**
	 * Method to register an account in the database
	 * 
	 * @param {String} login the login
	 * @param {String} email the email adress
	 * @param {String} password the password
	 * @param {successCallback} success the callback that handles the success of register the account 
	 * @param {failedCallback} failed the callback that handles the failed of register the account
	 */
	register(login, email, password, success, failed) {
		this.connection.connect();

		this.connection.query("INSERT INTO accounts (login, email, password) VALUES(?, ?, ?)",
			[login, email, password],
			(err, rows, field) => {
				if (!err) {
					success();
				} else {
					failed();
				}
			});
		
		this.connection.end();
	}

	/**
	 * Method to unregister an account in the database
	 * 
	 * @param {String} login the login
	 * @param {successCallback} success the callback that handles the success of unregister the account 
	 * @param {failedCallback} failed the callback that handles the failed of unregister the account
	 */
	unregister(login, success, failed) {
		this.connection.connect();

		this.connection.query("DELETE FROM accounts WHERE login = ?",
			[login],
			(err, rows, field) => {
				if (!err) {
					success();
				} else {
					failed();
				}
			});
		
		this.connection.end();
	}

	/**
	 * Callback for success action
	 * @callback successCallback
	 */

	/**
	 * Callback for failed action
	 * @callback failedCallback
	 */
}