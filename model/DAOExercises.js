var fs = require('fs');
var mysql = require('mysql');

module.exports = class DAOExercises {
	constructor() {
		var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
		this.connection = mysql.createConnection({
			host: config.database.host,
			database: config.database.database,
			user: config.database.user,
			password: config.database.password
		});
	}

	/**
	 * Method to read the list of exercises stored in the database
	 * 
	 * @param {exercisesCallback} cb the function called on success 
	 */
	getExercises(cb) {
		this.connection.connect();

		this.connection.query("SELECT * FROM exercises",
			(err, rows, field) => {
				if (!err && rows && rows.length > 0) {
					cb(rows);
				} else {
					cb([]);
				}
			});

		this.connection.end();
	}

	/**
	 * Method to read the list of exercises stored in the database
	 * 
	 * @param {Int} train 1 get training exercises, 0 get challenge exercises
	 * @param {exercisesCallback} cb the function called on success 
	 */
	getExercisesByType(train, cb) {
		this.connection.connect();

		// If value of train incorrect return empty array
		if (train != 0 && train != 1) {
			cb([]);
		} else {
			// Else get list of exercises
			this.connection.query("SELECT * FROM exercises WHERE train = ?",
				[train],
				(err, rows, field) => {
					if (!err && rows && rows.length > 0) {
						cb(rows);
					} else {
						cb([]);
					}
				});
		}
		this.connection.end();
	}

	/**
	 * Method to read the list of exercises stored in the database
	 * 
	 * @param {Int} level the level 1 (easy) to 10 (hard)
	 * @param {exercisesCallback} cb the function called on success 
	 */
	getChallengeExercisesByLevel(level, cb) {
		this.connection.connect();

		// If value of level incorrect return empty array
		if (level < 1 || level > 10) {
			cb([]);
		} else {
			// Else get list of exercises
			this.connection.query("SELECT * FROM exercises WHERE train = 0 AND level ?",
				[level],
				(err, rows, field) => {
					if (!err && rows && rows.length > 0) {
						cb(rows);
					} else {
						cb([]);
					}
				});
		}

		this.connection.end();
	}

	/**
	 * Method to read the exercise stored in the database
	 * 
	 * @param {Int} id the id of the exercise
	 * @param {exercisesCallback} cb the function called on success 
	 */
	getExerciseById(id, cb) {
		this.connection.connect();

		this.connection.query("SELECT * FROM exercises WHERE id = ?",
			[id],
			(err, rows, field) => {
				if (!err && rows && rows.length > 0) {
					cb(rows);
				} else {
					cb([]);
				}
			});

		this.connection.end();
	}

	/**
	 * Method to create an exercise
	 * @param {String} name the name of the exercise
	 * @param {String} wording the wording of the exercise (max 65,535 characters) 
	 * @param {String} testsGeneratorLink the URI of the test generator on the server 
	 * @param {String} solutionScriptLink the URI of the solution script on the server 
	 * @param {String} language the language to use for execute the two scripts
	 * @param {Int} train 1 if it is a training exercise, else 0
	 * @param {Int} level the level 1 (easy) to 10 (hard)
	 * @param {successActionCallback} success the callback that handles the success of exercise creation
	 * @param {failedActionCallback} failed the callback that handles the echec of exercise creation
	 */
	create(name, wording, testsGeneratorLink, solutionScriptLink, language, train, level, success, failed) {
		this.connection.connect();

		this.connection.query("INSERT INTO exercises (name, wording, testsGeneratorLink, solutionScriptLink, language, train, level) VALUES(?, ?, ?, ?, ?, ?, ?)",
			[name, wording, testsGeneratorLink, solutionScriptLink, language, train, level],
			(err, rows, field) => {
				if (!err) {
					success();
				} else {
					console.log([name, wording, testsGeneratorLink, solutionScriptLink, language, train, level]);
					console.log(err);
					failed();
				}
			});

		this.connection.end();
	}

	/**
	 * Method to create an exercise
	 * @param {String} name the name of the exercise
	 * @param {successActionCallback} success the callback that handles the success of exercise deletion
	 * @param {failedActionCallback} failed the callback that handles the echec of exercise deletion
	 */
	remove(name, success, failed) {
		this.connection.connect();

		this.connection.query("DELETE FROM exercises WHERE name = ?",
			[name],
			(err, rows, field) => {
				if (!err) {
					success();
				} else {
					failed();
				}
			});

		this.connection.end();
	}

	/**
	 * Callback called after get list of exercises
	 * @callback exercisesCallback
	 * @param {Array} rows the list of exercises
	 */

	/**
	 * Callback called after action was done
	 * @callback successActionCallback
	 */

	/**
	 * Callback called after action failed
	 * @callback failedActionCallback
	 */
}