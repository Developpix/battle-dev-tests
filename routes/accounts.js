var express = require('express');
const crypto = require('crypto');

var router = express.Router();
var DAOAccounts = require('../model/DAOAccounts');

router.get(['/', '/index'], (req, res, next) => {
  res.render('accounts/index', { title: 'Battle Dev - Comptes', account: req.session.account });
})
  .get('/connect', (req, res, next) => {
    res.render('accounts/connect', { title: 'Battle Dev - Inscription' });
  })
  .post('/connect', (req, res, next) => {
    var model = new DAOAccounts();

    // Create a reload function
    var reload = () => {
      res.render('accounts/connect', {
        title: 'Battle Dev - Inscription',
        login: req.body.login
      });
    };

    // If login or password is empty
    if (!req.body.login || !req.body.password) {
      reload();

    // Else try authentication
    } else {
      // Create sha512 hash method
      var hash = crypto.createHash('sha512');
      // Hash password
      hash.update(req.body.password);

      // Try authentication with the hash
      model.validateIdentity(req.body.login, hash.digest('hex'),
        () => {
          // Define account into session
          req.session.account = req.body.login;
          res.redirect('/accounts/index');
        },
        reload);
    }
  })
  .get('/add', (req, res, next) => {
    res.render('accounts/add', { title: 'Battle Dev - Inscription' });
  })
  .post('/add', (req, res, next) => {
    var model = new DAOAccounts();

    // Create a reload function
    var reload = () => {
      res.render('accounts/add', {
        title: 'Battle Dev - Inscription',
        login: req.body.login
      });
    };

    // If login or password or email is empty
    if (!req.body.login || !req.body.password || !req.body.email) {
      reload();

    // Else try register account
    } else {
      // Create sha512 hash method
      var hash = crypto.createHash('sha512');
      // Hash password
      hash.update(req.body.password);

      // Try register account with the hashed password
      model.register(req.body.login, req.body.email, hash.digest('hex'),
        () => {
          req.session.account = req.body.login;
          res.redirect('/accounts');
        },
        reload);
    }
  })
  .get('/remove', (req, res, next) => {
    var model = new DAOAccounts();
    
    // If the client is not connected redirect to welcome page 
    if (!req.session.account) {
      res.redirect('/index');
    }

    // Else remove account from database
    model.unregister(req.session.account,
      () => {
        // Disconnect client
        delete req.session.account;
        res.render('accounts/remove', {title: 'Battle Dev - Désinscription'});
      },
      () => {
        res.render('accounts/remove', {title: 'Battle Dev - Désinscription'});
      });
  })
  .get('/disconnect', (req, res, next) => {
    // Disconnect client
    delete req.session.account;

    // Redirect to welcome page
    res.redirect('/index');
  });

module.exports = router;
