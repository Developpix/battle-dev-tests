var express = require('express');
var router = express.Router();

/* GET home page. */
router.get(['/', '/index'], (req, res, next) => {
  if (req.session.account) {
    res.render('index', { title: 'IUT Battle Dev', account: req.session.account });
  } else {
    res.render('index', { title: 'IUT Battle Dev' });
  }
});

module.exports = router;
