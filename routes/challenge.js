var express = require('express');
var router = express.Router();

router.get('/', (req, res, next) => {
    res.render('challenge/index', { title: 'IUT Battle Dev - Compétition', account: req.session.account });
});

module.exports = router;