var express = require('express');
var multer = require('multer');
var fs = require('fs-extra')
var DAOExercises = require('../model/DAOExercises');

var upload = multer({ dest: 'tmp/uploads/' });
var router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) => {
  var model = new DAOExercises();

  model.getExercisesByType(1, (result) => {
    res.render('exercises/index', {
      title: 'IUT Battle Dev - Entraînements',
      account: req.session.account,
      exercises: result
    });
  });
})
  .get('/add', (req, res, next) => {
    // If client is connected
    if (req.session.account) {
      res.render('exercises/add', {
        title: "IUT Battle Dev - Création d'exercice",
        account: req.session.account
      });
    } else {
      // Else it is redirect to welcome page
      res.redirect('/index');
    }
  })
  .post('/add', upload.any(), (req, res, next) => {
    // Create a function to display again the page
    var reload = () => {
      res.render('exercises/add', {
        title: "IUT Battle Dev - Création d'exercice",
        account: req.session.account,
        name: req.body.name,
        wording: req.body.wording
      });
    };

    // If one field is empty or client send less than 2 files, display again the page
    if (!req.body.name || !req.body.wording || !req.body.language || !req.files[0] || !req.files[1]) {
      reload();
    } else {
      var model = new DAOExercises();

      // Else create the exercise in the database
      model.create(req.body.name,
        req.body.wording,
        'exercises/' + req.body.name + '/testsGenerator.' + req.files[0].originalname.split('.')[1],
        'exercises/' + req.body.name + '/solutionScript.' + req.files[0].originalname.split('.')[1],
        req.body.language,
        1, 1,
        () => {
          // On success, create the exercice folder
          fs.mkdir('exercises/' + req.body.name, { recursive: true }, (err) => {
            if (!err) {
              // If no error, move the first file in the exercise folder
              var name = req.files[0].fieldname + '.' + req.files[0].originalname.split('.')[1];
              fs.rename(req.files[0].path, 'exercises/' + req.body.name + '/' + name, (err) => {
                if (err) {
                  // If error reload page
                  reload();
                } else {
                  // Else move the second file in the exercise folder
                  name = req.files[1].fieldname + '.' + req.files[1].originalname.split('.')[1];
                  fs.rename(req.files[1].path, 'exercises/' + req.body.name + '/' + name, (err) => {
                    if (err) {
                      // If error reload page
                      reload();
                    } else {
                      // Else redirect to welcome page
                      res.redirect('/index');
                    }
                  });
                }
              });
            } else {
              // Else if error reload page
              reload();
            }
          })
        },
        reload);
    }
  })
  .get('/remove', (req, res, next) => {
    var model = new DAOExercises();

    model.getExercises((exercises) => {
      res.render('exercises/remove', {
        title: 'IUT Battle Dev - Suppresion exercice',
        account: req.session.account,
        exercises: exercises
      })
    });
  })
  .post('/remove', (req, res, next) => {
    var model = new DAOExercises();

    model.remove(req.body.exercise, () => {
      fs.remove('exercises/' + req.body.exercise, (err) => {
        if (err) {
          // If there is an error display again the page but with just the exercise and an error message
          res.render('exercises/remove', {
            title: 'IUT Battle Dev - Suppresion exercice',
            account: req.session.account,
            exercises: [req.body.exercise],
            error: "Impossible de supprimer l'exercice " + req.body.exercise
          });
        } else {
          // If no problem, redirect to exercises page
          res.redirect('/exercises');
        }
      });
    },
      () => {
        // If there is an error display again the page but with just the exercise and an error message
        res.render('exercises/remove', {
          title: 'IUT Battle Dev - Suppresion exercice',
          account: req.session.account,
          exercises: [req.body.exercise],
          error: "Impossible de supprimer l'exercice " + req.body.exercise
        });
      })

  })
  .get('/:exercise', (req, res, next) => {
    var languages = JSON.parse(fs.readFileSync('languages.json', 'utf8'));
    var model = new DAOExercises();

    // Load exercise from the database
    model.getExerciseById(req.params.exercise, (rows) => {
      // If no exercise associated to this id redirect to exercises page
      if (rows.length < 1) {
        res.redirect('/exercises');
      } else {
        // Else display the exercise page
        var md = require('marked');

        // If the exercise is not a training exercise redirect to exercises page
        if (rows[0].train != 1) {
          res.redirect('/exercises');
        } else {
          // Else display the exercise
          res.render('exercises/exercise', {
            title: 'IUT Battle Dev - Exercice ' + req.params.exercise,
            exercise: rows[0],
            languages: languages,
            md: md
          });
        }
      }
    });

    req.io.sockets.on('connection', function (socket) {
      socket.on('message', (data) => {
        console.log(data);
        switch (data.language) {
          case 'c':
            name = 'exercise.c';
            break;
          case 'c++':
            name = 'exercise.cpp';
            break;
          case 'csharp':
            name = 'exercise.cs';
            break;
          case 'java':
            name = 'Application.java';
            break;
          case 'haskell':
            name = 'exercise.hs';
            break;
          case 'javascript':
            name = 'exercise.js';
            break;
          case 'php':
            name = 'exercise.php';
            break;
          case 'python':
            name = 'exercise.py';
            break;
          case 'ruby':
            name = 'exercise.rb';
            break;
          default:
            break;
        }

        fs.mkdir('exercises/train/' + req.session.account, { recursive: true }, (err) => {
          fs.writeFile("exercises/train/" + req.session.account + '/' + name, data.code, function (err) {
            if (err) {
              return console.log(err);
            }
            
            switch (data.language) {
              case 'c':
                var gcc = require('child_process').spawn("gcc", ["-Wall", "-Wextra", "-pedantic", "exercises/train/" + req.session.account + "/exercise.c"])

                var stderr = "";
                gcc.stderr.on('data', (data) => {
                  stderr += data.toString();
                });

                gcc.on('close', (code, signal) => {
                  if (code == 1) {
                    socket.emit('stderr', stderr.replace(/\n/gi, "<br />\n"));
                  } else {
                    var prog = require('child_process').spawn("./a.out");

                    prog.stdin.write("hello");

                    var stdout = "";
                    prog.stdout.on('data', (data) => {
                      stdout += data.toString();
                    });

                    prog.stderr.on('data', (data) => {
                      stderr += data.toString();
                    });

                    prog.stdin.end();

                    prog.on('close', (code, signal) => {
                      socket.emit('stdout', stdout.replace(/\n/gi, "<br />\n"));
                      socket.emit('stderr', stderr.replace(/\n/gi, "<br />\n"));
                    });
                  }
                });
                break;
              case 'c++':
                // TODO
                break;
              case 'csharp':
                // TODO
                break;
              case 'java':
                var javac = require('child_process').spawn("javac", ["-d", "bin/", "exercises/train/" + req.session.account + "/Application.java"])

                var stderr = "";
                javac.stderr.on('data', (data) => {
                  stderr += data.toString();
                });

                javac.on('close', (code, signal) => {
                  if (code == 1) {
                    socket.emit('stderr', stderr.replace(/\n/gi, "<br />\n"));
                    socket.emit('stdout', '');
                  } else {
                    var java = require('child_process').spawn("java", ["-cp", "bin/:.", "Application"]);

                    java.stdin.write("hello");

                    var stdout = "";
                    java.stdout.on('data', (data) => {
                      stdout += data.toString();
                    });

                    java.stderr.on('data', (data) => {
                      stderr += data.toString();
                    });

                    java.stdin.end();

                    java.on('close', (code, signal) => {
                      socket.emit('stdout', stdout.replace(/\n/gi, "<br />\n"));
                      socket.emit('stderr', stderr.replace(/\n/gi, "<br />\n"));
                    });
                  }
                });
                break;
              case 'haskell':
                  var ghc = require('child_process').spawn("ghc", ["-dynamic", "exercises/train/" + req.session.account + "/exercise.hs"])
  
                  var stderr = "";
                  ghc.stderr.on('data', (data) => {
                    stderr += data.toString();
                  });
  
                  ghc.on('close', (code, signal) => {
                    if (code == 1) {
                      socket.emit('stderr', stderr.replace(/\n/gi, "<br />\n"));
                      socket.emit('stdout', '');
                    } else {
                      var prog = require('child_process').spawn("exercises/train/" + req.session.account + "/exercise");
  
                      prog.stdin.write("hello");
  
                      var stdout = "";
                      prog.stdout.on('data', (data) => {
                        stdout += data.toString();
                      });
  
                      prog.stderr.on('data', (data) => {
                        stderr += data.toString();
                      });
  
                      prog.stdin.end();
  
                      prog.on('close', (code, signal) => {
                        socket.emit('stdout', stdout.replace(/\n/gi, "<br />\n"));
                        socket.emit('stderr', stderr.replace(/\n/gi, "<br />\n"));
                      });
                    }
                  });
                  break;
              case 'javascript':
                var prog = require('child_process').spawn("node", ["exercises/train/" + req.session.account + "/exercise.js"]);
                
                prog.stdin.write("hello");

                var stdout = "";
                prog.stdout.on('data', (data) => {
                  stdout += data.toString();
                });

                var stderr = "";
                prog.stderr.on('data', (data) => {
                  stderr += data.toString();
                });

                prog.stdin.end();
                
                prog.on('close', (code, signal) => {
                  console.log(code + '\n' + signal);
                  socket.emit('stdout', stdout.replace(/\n/gi, "<br />\n"));
                  socket.emit('stderr', stderr.replace(/\n/gi, "<br />\n"));
                });
                break;
              case 'php':
                var php = require('child_process').spawn("php", ["exercises/train/" + req.session.account + "/exercise.php"]);
  
                php.stdin.write("hello");

                var stdout = "";
                php.stdout.on('data', (data) => {
                  stdout += data.toString();
                });

                var stderr = "";
                php.stderr.on('data', (data) => {
                  stderr += data.toString();
                });

                php.stdin.end();

                php.on('close', (code, signal) => {
                  socket.emit('stdout', stdout.replace(/\n/gi, "<br />\n"));
                  socket.emit('stderr', stderr.replace(/\n/gi, "<br />\n"));
                });
                break;
              case 'python':
                var python = require('child_process').spawn("python", ["exercises/train/" + req.session.account + "/exercise.py"]);
  
                python.stdin.write("hello");

                var stdout = "";
                python.stdout.on('data', (data) => {
                  stdout += data.toString();
                });

                var stderr = "";
                python.stderr.on('data', (data) => {
                  stderr += data.toString();
                });

                python.stdin.end();

                python.on('close', (code, signal) => {
                  socket.emit('stdout', stdout.replace(/\n/gi, "<br />\n"));
                  socket.emit('stderr', stderr.replace(/\n/gi, "<br />\n"));
                });
                break;
              case 'ruby':
                var ruby = require('child_process').spawn("ruby", ["exercises/train/" + req.session.account + "/exercise.rb"]);
    
                ruby.stdin.write("hello");

                var stdout = "";
                ruby.stdout.on('data', (data) => {
                  stdout += data.toString();
                });

                var stderr = "";
                ruby.stderr.on('data', (data) => {
                  stderr += data.toString();
                });

                ruby.stdin.end();

                ruby.on('close', (code, signal) => {
                  socket.emit('stdout', stdout.replace(/\n/gi, "<br />\n"));
                  socket.emit('stderr', stderr.replace(/\n/gi, "<br />\n"));
                });
                break;
              default:
                break;
            }

          });
        });

      });
    });
  });

module.exports = router;
