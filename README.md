# Battle Dev
Ceci est un dépôt permettant de créer un serveur pour gérer une Battle Dev.

# Configuration
créer un fichier de configuration à la racine, `config.json`
pour configurer le serveur, en suivant la structure ci-dessous :
```json
{
	"database": {
		"host": "adresse de la base de données",
		"database": "la base de données",
		"user": "l'utilisateur",
		"password": "son mot de passe"
	}
}
```
