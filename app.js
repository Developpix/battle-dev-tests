var createError = require('http-errors');
var express = require('express');
var path = require('path');
var compass = require('node-compass');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');

var app = express();
var httpServer = require('http').createServer(app);
var io = require('socket.io')(httpServer);

var indexRouter = require('./routes/index');
var accountsRouter = require('./routes/accounts');
var exercisesRouter = require('./routes/exercises');
var challengeRouter = require('./routes/challenge');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(compass({ mode: 'expanded' }));
app.use(express.static(path.join(__dirname, 'public')));
app.set('trust proxy', 1);
app.use(session({
  name: 'IUT - Battle Dev',
  secret: 'La réponses est 42',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}));

app.use((req, res, next) => {
  req.io = io;
  next();
});
app.use('/', indexRouter);
app.use('/accounts', accountsRouter);
app.use('/exercises', exercisesRouter);
app.use('/challenge', challengeRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = {app: app, server: httpServer};